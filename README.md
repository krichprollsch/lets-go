# Let's Go

Talk given during the Clermont'ech [APIHour#45](https://www.clermontech.org/api-hours/api-hour-45.html).

https://krichprollsch.gitlab.io/lets-go

Thanks to [remark](https://remarkjs.com/#1).
