- quels sont les critères de choix d'un langage dans une entreprise ?
    * https://americanexpress.io/choosing-go/
    * https://medium.com/@kevalpatel2106/why-should-you-learn-go-f607681fad65
- projets "annexes" entre nodejs vs go
- https://www.gowitek.com/golang/blog/companies-using-golang


# Let's Go

## Somes questions

- Qui n'a jamais entendu parlé du langage Go ?
- Qui a déjà codé quelque chose en Go ?

## Go turns 10

https://blog.golang.org/10years


## What is Go?

> Go is a compiled, concurrent, garbage-collected, statically typed language developed at Google.
> It is an open source project.
Rob Pike - https://talks.golang.org/2012/splash.article

## Why should I choose Go?

### Disclaimer

Bien sûr je parle mieux de ce que je connais, avant de faire du Go, j'ai codé
essentiellement avec PHP, avec quelques incursions dans le JS (node) et puis
il y'a plus longtemps du Java.
Certains des points de cette présentation s'appliquent d'autant plus avec cette expérience

Je ne travaille avec Go que depuis 1 an, je ne sais pas tout ^^

### Go has been created to ease software engineering

> Software engineering is what happens to programming
> when you add time and other programmers.
Russ Cox - https://research.swtch.com/vgo-eng

> Go is a programming language designed by Google to help solve Google's problems,
> and Google has big problems.
> [...]
> The language was designed by and for people who write—and read and debug and
> maintain—large software systems.
Rob Pike - https://talks.golang.org/2012/splash.article

- la programmation c'est résoudre un problème en écrivant du code
- programmer est dur en soi
- le génie logiciel c'est ajouter à la programmation:
    - le contrainte de le faire fonctionner à long terme
    - plusieurs développeurs pour le modifier
    - mettre en place un VCS
    - architecturer le code pour le rendre modulaire, suivre des design patterns
    - faciliter la dtecton de bugs: utiliser une CI, des outils d'analyse
    - écrire des tests pour garantir des non-regressions

Go a été designé dans le but d'être meilleur ça:
- le formatage est obligatoire et inclu, c'est pas forcément "le meilleur" mais il n'y en a qu'un
    - pas de discussions sur le format à suivre

![silicon-valley - spaces tabs](https://media.giphy.com/media/l0IylSajlbPRFxH8Y/giphy.gif)

    - facilité de lecture quelle que soit l'origine du code : projet, dépendance, core
    - **un code formatté facilite la création d'outils**
        - goimports
        - gorename
        - go fix

- imports are urls
    - lorsque vous lisez un fichier go, toutes ses dépendances sont déclarées dans un statement `import` au debut du fichier
    - l'utilisation d'urls enlève toute ambiguité sur la source de la dépendance
    - facilite l'accès à ce code source

https://talks.golang.org/2012/splash.article

#### Go is compiled

Go est un langage compilé
- le temps de compilation est hyper rapide
- on peut l'utiliser en dev quasiment comme un langage interprété
- `go run` compile et run
- un binaire compilé: deploiement simple et rapide
- on peut compiler le programme vers n'importe quelle arch

```
todo bo guild example
```

- **no warmup time**

### Go is easy to install and update

- download, decompress, code
- run on every arch (windows, mac, linux)
- doc, unit tests, deps manager included
- no need to have docker, a VM, ...

https://golang.org/doc/install

### Go is easy to learn

- C family
- beaucoup d'articles dans le blog officiel et parmis la core team extrèment précis, complets et bien rédigés
    - le blog est une mine d'or
- plusieurs livres qui font référence dans le langage
- le tour de Go
- doc inclue et cohérente (à la javadoc)
- la "marche" est beaucoup moins haute que pour d'autres langages récents (Rust, fonctionnels, etc...)
- garbage collector
- **no sugar**
- la core lib est là aussi pour apprendre et comprendre

> Some programmers find it fun to work in; others find it unimaginative, even boring.
Rob Pike - https://talks.golang.org/2012/splash.article

https://golang.org/doc/install
https://golang.org/doc/
https://blog.golang.org/

J'y arrive, tout dev peut le faire !

### Go run fast

- quand tu viens d'un langage interprété, ça se pose là...
- +-10% du C
- ok c'est moins bien que Rust
- il est facile à scaler, merci les goroutines
- garbage collector très efficace

https://blog.golang.org/ismmkeynote

### Go encourage best practices

### Go core

- lib standard
- network
- garbage collector

### Go ease the use of concurrent programming

C'est une révolution !

```
go letsgo()
```

- c'est super light, on peut avoir des milliers de goroutines sans problème
- les channel + select pour communiquer entre les goroutines
- quelques bonne pratiques pour (moins) se tromper
    - toujours prévoir quand une goroutine se termine
    - attention aux data races
    - drainer les channels (les goroutines ça leake)
    - utiliser les context pour terminer des goroutines
- commencer sans et les mettre en place si nécessaire

https://talks.golang.org/2012/waza.slide

https://golang.org/doc/articles/race_detector.html
https://blog.golang.org/context

### On the shoulders of giants

Robert Griesemer, Rob Pike and Ken Thompson
Ian Taylor, Russ Cox

https://golang.org/doc/faq#history

### Backward compatible

Go garantit qu'un programme écrit dans une version tournera toujours sur les version
supérieures (l'inverse au faux).

https://golang.org/doc/go1compat

### The Gopher is cool

https://golang.org/doc/faq#gopher

### Friendly reminder

C'est le langage "Go".
"golang" est utilisé pour faciliter le réferencement sur les moteurs de recherche.

https://golang.org/doc/faq#go_or_golang

### Flaws

Tout n'est pas rose non plus:
- la conccurence, même facilitée, reste touchy à prendre en main au début
- le typage statique est un exercice auquel il faut s'habituer quand on vient de la souplesse dynamique
- utiliser reflect est pas évident au début
- que se passera-t'il si Go 2 sort un jour ?
- la gestion des erreurs est souvent critiquée (mais moi je l'aime bien comme ça <3)

### Conclusion

Go est un language qui a tout les arguments pour s'intégrer dans une stack existante
ou nouvelle dans une entreprise.
Il y'a souvent des fênetres qui demande l'introduction d'une nouvelle technologie
et Go est un choix à prendre en compte qui sera payant très rapidement.

Un exemple est l'utilisation de nodejs dans une stack pour gérer des websockets.
Ses performances, les libs dispo et la facilité de mise en place font de Go un choix
pertinent pour ce genre choses.


https://americanexpress.io/choosing-go/
